const createError = require('http-errors'),
    express = require('express'),
    path = require('path'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    sassMiddleware = require('node-sass-middleware');

const indexRouter = require('./routes/index'),
    usersRouter = require('./routes/users');

const app = express();

app
.set('views', path.join(__dirname, 'views'))
.set('view engine', 'ejs');

app.use(logger('short'))
.use(express.json())
.use(express.urlencoded({ extended: false }))
.use(cookieParser())
.use(sassMiddleware({
    src: path.join(__dirname, 'public/styles/scss'),
    dest: path.join(__dirname, 'public/styles/css'),
    indentedSyntax: false, // true = .sass and false = .scss
    sourceMap: false,
    debug: false,
    prefix: '/styles/css/',
    outputStyle: 'compressed'
}))
.use(express.static(path.join(__dirname, 'public')));

app
.use('/', indexRouter)
.use('/users', usersRouter);

app.use((req, res, next) => next(createError(404)));

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500).render('error');
});

module.exports = app;