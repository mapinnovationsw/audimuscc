const express = require('express'),
    axios = require('axios'),
    router = express.Router(),
    path = require("path"),
    fs = require('fs'),
    xmlBuilder = require('xmlbuilder'),
    xml2js = require('xml2js');

router
    .get('/teste', (req, res, next) => {
        var parser = new xml2js.Parser();
        fs.readFile(__dirname + '../../public/cc/teste.xml', function (err, data) {
            parser.parseString(data, function (err, result) {
                // console.log(result);
                // res.send(result);
                res.render('index', {
                    result
                });
            });
        });
    })
    .get('/', function (req, res, next) {
        res.render('index', {
            title: 'Express'
        });
    });

module.exports = router;